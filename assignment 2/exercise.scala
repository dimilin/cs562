//input
val pagecounts = sc.textFile("file:///C:/Users/dimitris/Desktop/spark-2.3.2-bin-hadoop2.7/spark-2.3.2-bin-hadoop2.7/bin/pagecounts-20160101-000000_parsed.out")

//task 1
def Task1(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 rdd.take(15).foreach(println)
}

//task 2
def Task2(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val c=rdd.count()
 println("The number of records the dataset has in total: " + c)
}

//task 3
def Task3(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val pageSizeList = rdd.map{x => x.split(" ")}.map{x => x(3).toLong}
 println("Highest page size:" + pageSizeList.max)
 println("Lowest page size:" + pageSizeList.min)
 val averagePageSize = pageSizeList.reduce(_ + _) / pageSizeList.count()
 println("Average page size:" + averagePageSize)
}

//task 4
def Task4(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(3).toLong,x(0)+" "+x(1)+" "+x(2))}
 val maxRecord = record.groupByKey.map(x => (x._1, x._2.toList)).takeOrdered(1)(Ordering[Long].reverse.on(_._1))
 maxRecord.foreach{case(x,y)=> println("The record with the largest page size: " + y + " page size: " + x)}
}

//task 5
def Task5(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 //create pairs with key: page size, value: project code + page title  + num hits 
 val record = rdd.map{x => x.split(" ")}.map{x => (x(3).toLong,x(0)+" "+x(1)+" "+x(2))}
 //take pairs with max page size
 val pairsWithKeyPageSize = record.groupByKey.map(x => (x._1, x._2.toList)).takeOrdered(1)(Ordering[Long].reverse.on(_._1))
 //create pairs with key: num hits, value: project code + page title
 val pairsWithKeyNumHits = pairsWithKeyPageSize.flatMap{case(x,y) => y}.map{_.split(" ")}
 .map( x => (x(2).toInt,x(0) + " " + x(1)))
 //sort pairs with key: num hits
 scala.util.Sorting.stableSort(pairsWithKeyNumHits, (e1: (Int, String), e2: (Int, String)) => e1._1 > e2._1)
 //take max from pairs
 print("Records with max page size: ")
 pairsWithKeyPageSize.foreach(println)
 print("The most popular: ")
 pairsWithKeyNumHits.take(1).foreach(println)
}


//task 6
def Task6(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(1).length,x(0)+" "+x(1)+" "+x(2)+" "+x(3))}
 val maxRecord = record.groupByKey.map(x => (x._1, x._2.toList)).takeOrdered(1)(Ordering[Int].reverse.on(_._1))
 maxRecord.foreach{case(x,y)=> println("The record with the largest page title: " + y + " page title: " + x)}
}

//task 7
def Task7(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(3).toLong,x(0)+" "+x(1)+" "+x(2)+" "+x(3))}.filter{ case(x,y) => x > 132239 }.take(10)
 record.foreach{case(x,y)=>println(y)}
}

//task 8
def Task8(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(0),x(2).toLong)}.reduceByKey{case(x,y)=>x+y}.take(10)
 record.foreach{println}
}

//task 9
def Task9(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(0),x(2).toLong)}.reduceByKey{case(x,y)=>x+y}.
 takeOrdered(10)(Ordering[Long].reverse.on(_._2))
 record.foreach{println}
}

//task 10
def Task10(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val record = rdd.map{x => x.split(" ")}.map{x => (x(1),x(0)+" "+x(1)+" "+x(2)+" "+x(3))}.
 filter{case(x,y) => x.startsWith("The")}
 val allWithThe = record.count()
 println("All page titles that start with the article 'The': " + allWithThe)
 //not en
 val notEnglish = record.filter{case(x,y) => !y.startsWith("en")}.count()
 println("Not English: " + notEnglish)
}

//task11
def Task11(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val singlePageCounter = rdd.map{x => x.split(" ")}.map{x => (x(2).toInt,x(0)+" "+x(1)+" "+x(2)+" "+x(3))}.
 filter{case(x,y) => x == 1}.count()
 val allRecords = rdd.count()
 println("Percentage of pages that have only received a single page view: " + (singlePageCounter.toDouble / allRecords.toDouble) * 100.toDouble)
}

//task12
def Task12(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val regex = "[a-zA-Z]+"
 val unique = rdd.map{x => x.split(" ")}.map{x => x(1).toLowerCase}.
 flatMap{x => x.split("_")}.filter{x => x.matches(regex)}.distinct().count()
 println("Unique words: " + unique)
}

//task 13
def Task13(rdd:org.apache.spark.rdd.RDD[String]): Unit = {
 val regex = "[a-zA-Z]+"
 val uniqueTerm = pagecounts.map{x => x.split(" ")}.map{x => x(1).toLowerCase}.
 flatMap{x => x.split("_")}.filter{x => x.matches(regex)}.map(word => (word,1)).reduceByKey(_+_).
 takeOrdered(1)(Ordering[Int].reverse.on(_._2))
 println("Unique term: " + uniqueTerm(0))
}