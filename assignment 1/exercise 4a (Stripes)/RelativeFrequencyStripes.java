package relativefrequencystripes.cs562;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;

public class RelativeFrequencyStripes extends Configured implements Tool {
  public static TreeMap<Text,Float> sortMap = new TreeMap<Text,Float>();
  private static final Logger LOG = Logger.getLogger(RelativeFrequencyStripes.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new RelativeFrequencyStripes(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
	  
    Job job1 = Job.getInstance(getConf(), "relative frequency");
    job1.setJarByClass(RelativeFrequencyStripes.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job1.setMapperClass(Map.class);
    //job.setCombinerClass(Reduce.class);
    job1.setReducerClass(Reduce.class);
    job1.setNumReduceTasks(20);
    
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(Text.class);
    
    FileInputFormat.setInputPaths(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, new Path(args[1], "out1"));
    
    //share the stopwords list 
    job1.addCacheFile(new Path("/home/cloudera/workspace/RelativeFrequencyStripes/stopwords.csv").toUri());
    
    job1.waitForCompletion(true);
    
    Job job2 = Job.getInstance(getConf(), "sort by value");
    job2.setJarByClass(RelativeFrequencyStripes.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job2.setMapperClass(MapSwapKeyValue.class);
    job2.setReducerClass(ReduceSwapKeyValue.class);
    job2.setNumReduceTasks(1);
    job2.setSortComparatorClass(ReverseComparator.class);
    
    job2.setOutputKeyClass(FloatWritable.class);
    job2.setOutputValueClass(Text.class);
    // set key and value as text input format
    job2.setInputFormatClass(KeyValueTextInputFormat.class);
    
    FileInputFormat.setInputPaths(job2, new Path(args[1], "out1"));
    FileOutputFormat.setOutputPath(job2, new Path(args[1], "out2"));
    
    
    return job2.waitForCompletion(true) ? 0 : 1;
    

  }

  public static class Map extends Mapper<LongWritable, Text, Text, Text> {
//    private final static IntWritable one = new IntWritable(1);
	private String regx = "[a-zA-Z]+";
    private boolean caseSensitive = false;
    
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

    protected void setup(Mapper.Context context)
        throws IOException,
        InterruptedException {
    	
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
      //stopwords
      URI[] localPaths = context.getCacheFiles();
      //parse stopwords
      parseSkipFile(localPaths[0]);
    }

    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }
    
    @Override
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      //String fileName = ((FileSplit) context.getInputSplit()).getPath().getName();
      String line = lineText.toString();
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      for (String word : WORD_BOUNDARY.split(line)) {
        if (word.isEmpty() || patternsToSkip.contains(word)) {
            continue;
        }
        if(word.matches(regx)){
        	HashMap<String,Integer> stripe = new HashMap<String,Integer>();
        	for (String term : WORD_BOUNDARY.split(line)) {
        		if (term.isEmpty() || patternsToSkip.contains(term)) {
                    continue;
                }
        		if(term.matches(regx) && !term.equals(word)){
        			Integer counter = stripe.get(term);
        			if(counter == null){
        				stripe.put(term, 1);
        			}else{
        				stripe.put(term,counter.intValue() + 1);
        			}
        		}
        	}
        	StringBuilder output = new StringBuilder();
        	for(String key : stripe.keySet()){
        		output.append(key);
        		output.append("=").append(stripe.get(key)).append(" ");
        	}
        	context.write(new Text(word), new Text(output.toString()));
        }
      }   
    }
    
  }
  
  public static class Reduce extends Reducer<Text, Text, Text, Text> {
	
    @Override
    public void reduce(Text word, Iterable<Text> stripes, Context context)
        throws IOException, InterruptedException {
    	HashMap<String,Integer> stripeMap = new HashMap<String,Integer>();
    	int totalCounter = 0;
    	
    	for( Text stripe : stripes){
    		String[] pairs = stripe.toString().split(" ");
    		for( String pair : pairs){
    			String[] pairSplit = pair.split("=");
    			if(pair.isEmpty()){
    				continue;
    			}
    			String key = pairSplit[0];
        		int value = Integer.parseInt(pairSplit[1]);
        		Integer counter = stripeMap.get(key);
        		if(counter == null){
        			stripeMap.put(key, value);
        		}else{
        			stripeMap.put(key, counter.intValue() + value);
        		}
        		totalCounter += value;
    		}
    	}
    	
    	
    	for(String keyInStripe : stripeMap.keySet()){
    		String outputKey = word + "," + keyInStripe;
    		float tmpOut = stripeMap.get(keyInStripe).intValue()/totalCounter;
    		context.write(new Text(outputKey),new Text(String.valueOf(tmpOut)));
    	}
    }
  }
  
  public static class MapSwapKeyValue extends Mapper<Text, Text, FloatWritable, Text> {
	    @Override
	    public void map(Text key, Text value, Context context ) throws IOException,InterruptedException{
	    	//swap key-value make freq as key
	    	float floatValue = Float.parseFloat(value.toString());
	    	context.write(new FloatWritable(floatValue), key);
	    }
	  }
	  
	  public static class ReduceSwapKeyValue extends Reducer<FloatWritable, Text, Text, FloatWritable> {	
		static int count = 1; 
		@Override
	    public void reduce(FloatWritable key, Iterable<Text> words, Context context)
	        throws IOException, InterruptedException {
			//for each freq we have a list of words so write in text a pair of <frequency , word >
			for(Text word : words){
				if(count < 101){
					context.write(new Text(word),key);
				}
				count++;
			}
	    }
	  }
	  
	  
	  
	  public static class ReverseComparator extends WritableComparator {
		     
		private static final Text.Comparator TEXT_COMPARATOR = new Text.Comparator();
	    public ReverseComparator() {
	        super(Text.class);
	    }
	 
	    @Override
	    public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
	       return (-1)* TEXT_COMPARATOR.compare(b1, s1, l1, b2, s2, l2);
	    }
	 
	    @Override
	    public int compare(WritableComparable a, WritableComparable b) {
	        if (a instanceof Text && b instanceof Text) {
	                return (-1)*(((Text) a).compareTo((Text) b));
	        }
	        return super.compare(a, b);
	    }
	  }
	  
}
  
  
