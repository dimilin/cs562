package invertedindexwithcounter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;

public class InvertedIndexWithCounter extends Configured implements Tool {
	
  private static final Logger LOG = Logger.getLogger(InvertedIndexWithCounter.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new InvertedIndexWithCounter(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
	  
    Job job1 = Job.getInstance(getConf(), "invertedindex");
    job1.setJarByClass(InvertedIndexWithCounter.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job1.setMapperClass(Map.class);
    //job1.setCombinerClass(Reduce.class);
    job1.setReducerClass(Reduce.class);
    job1.setNumReduceTasks(1);
    
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(Text.class);
    
    FileInputFormat.setInputPaths(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, new Path(args[1], "out1"));
    
    //share the stopwords list 
    job1.addCacheFile(new Path("/home/cloudera/workspace/InvertedIndexWIthCounter/stopwords.csv").toUri());
    
    job1.waitForCompletion(true);
    
    Job job2 = Job.getInstance(getConf(), "invertedindex");
    job2.setJarByClass(InvertedIndexWithCounter.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job2.setMapperClass(SecondMap.class);
    job2.setReducerClass(SecondReduce.class);
    job2.setNumReduceTasks(1);
    
    job2.setOutputKeyClass(Text.class);
    job2.setOutputValueClass(Text.class);
 // set key and value as text input format
    job2.setInputFormatClass(KeyValueTextInputFormat.class);
    
    FileInputFormat.setInputPaths(job2, new Path(args[1], "out1"));
    FileOutputFormat.setOutputPath(job2, new Path(args[1], "out2"));
    
    return job2.waitForCompletion(true) ? 0 : 1;
    
  }

  public static class Map extends Mapper<LongWritable, Text, Text, Text> {
//    private final static IntWritable one = new IntWritable(1);
	private String regx = "[a-zA-Z]+";
    private boolean caseSensitive = false;
    
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

    protected void setup(Mapper.Context context)
        throws IOException,
        InterruptedException {
    	
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
      //stopwords
      URI[] localPaths = context.getCacheFiles();
      //parse stopwords
      parseSkipFile(localPaths[0]);
    }

    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }
    
    @Override
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String fileName = ((FileSplit) context.getInputSplit()).getPath().getName();
      String line = lineText.toString();
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      for (String word : WORD_BOUNDARY.split(line)) {
        if (word.isEmpty() || patternsToSkip.contains(word)) {
            continue;
        }
        if(word.matches(regx)){
        	context.write(new Text(word), new Text(fileName));
        }
      }   
    }
  }
  
  public static class Reduce extends Reducer<Text, Text, Text, Text> {
	
    @Override
    public void reduce(Text word, Iterable<Text> docs, Context context)
        throws IOException, InterruptedException {
      //stringbuilder for list of docs
      StringBuilder sb = new StringBuilder();
      //for each doc build the string of docs
      for (Text doc : docs) {
    	  String docName = doc.toString();
		  sb.append(docName);
    	  sb.append(" ");
      }
      //write word,stringBuilder(list of docs)
      context.write(word, new Text(sb.toString()));
      
    }
  }
  
  public static class SecondMap extends Mapper<Text, Text, Text, Text> {
    @Override
    public void map(Text word, Text docList, Context context ) throws IOException,InterruptedException{
    	HashMap<String, Integer> docMap = new HashMap<String, Integer>();
    	//iterate the list of documents
    	for (String docName: docList.toString().split(" ")){
    		
    		if(docMap.get(docName) == null){
    			docMap.put( docName, 1);
    		}else{
    			int counterOfDocName = docMap.get(docName);
    			docMap.put(docName, ++counterOfDocName);
    		}
    	}
    	StringBuilder output = new StringBuilder();
    	for(String doc : docMap.keySet()){
    		output.append(doc);
    		output.append(" #").append(docMap.get(doc)).append("  ");
    	}
    	context.write(word , new Text(output.toString()));
    }
  }
  
  public static class SecondReduce extends Reducer<Text, Text, Text, Text> {
	private static int id =1;
	@Override
    public void reduce(Text word, Iterable<Text> docs, Context context)
        throws IOException, InterruptedException {
		//for each freq we have a list of words so write in text a pair of <frequency , word >
		for(Text doc : docs){
			context.write(new Text(id + " " + word.toString()),doc);
		}
		id++;
    }
  }
  
  
}
