package invertedindex.cs562;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;

public class InvertedIndex extends Configured implements Tool {
	
  private static final Logger LOG = Logger.getLogger(InvertedIndex.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new InvertedIndex(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
	  
    Job job = Job.getInstance(getConf(), "invertedindex");
    job.setJarByClass(InvertedIndex.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job.setMapperClass(Map.class);
    //job.setCombinerClass(Reduce.class);
    job.setReducerClass(Reduce.class);
    job.setNumReduceTasks(1);
    
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    
    FileInputFormat.setInputPaths(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1], "out1"));
    
    //share the stopwords list 
    job.addCacheFile(new Path("/home/cloudera/workspace/InvertedIndex/stopwords.csv").toUri());
    
    return job.waitForCompletion(true) ? 0 : 1;

  }

  public static class Map extends Mapper<LongWritable, Text, Text, Text> {
//    private final static IntWritable one = new IntWritable(1);
	private String regx = "[a-zA-Z]+";
    private boolean caseSensitive = false;
    
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

    protected void setup(Mapper.Context context)
        throws IOException,
        InterruptedException {
    	
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
      //stopwords
      URI[] localPaths = context.getCacheFiles();
      //parse stopwords
      parseSkipFile(localPaths[0]);
    }

    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }
    
    @Override
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String fileName = ((FileSplit) context.getInputSplit()).getPath().getName();
      String line = lineText.toString();
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      for (String word : WORD_BOUNDARY.split(line)) {
        if (word.isEmpty() || patternsToSkip.contains(word)) {
            continue;
        }
        if(word.matches(regx)){
        	context.write(new Text(word), new Text(fileName));
        }
      }   
    }
  }
  
  public static class Reduce extends Reducer<Text, Text, Text, Text> {
	enum MyCounters{WordOnOneDocument};
	private static int id =1;
	
    @Override
    public void reduce(Text word, Iterable<Text> docs, Context context)
        throws IOException, InterruptedException {
      //counter to see if a word is in one document
      int count = 0;
      //stringbuilder for list of docs
      StringBuilder sb = new StringBuilder();
      //for each doc build the string of docs
      for (Text doc : docs) {
    	  String docName = doc.toString();
    	  //if doc is already in the stringBuilder dont put it again
    	  if(!sb.toString().contains(docName)){
    		  count++;
    		  sb.append(docName);
        	  sb.append(" ");
    	  }
      }
      if(count == 1){
    	  context.getCounter(MyCounters.WordOnOneDocument).increment(1); 
      }
      String idToString = id +" " + word.toString();
      id++;
      //write word,stringBuilder(list of docs)
      context.write(new Text(idToString), new Text(sb.toString()));
      
    }
  }
  
}
