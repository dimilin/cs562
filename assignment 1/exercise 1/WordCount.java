package wordcount.cs562;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;

public class WordCount extends Configured implements Tool {
	
  private static final Logger LOG = Logger.getLogger(WordCount.class);

  public static void main(String[] args) throws Exception {
	long start = System.currentTimeMillis();
    int res = ToolRunner.run(new WordCount(), args);
    long end = System.currentTimeMillis() - start;
    System.out.println("execution time: " + end);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
	Configuration conf = new Configuration(); 
	//conf.setBoolean("mapred.compress.map.output", true);
	//conf.set("mapred.map.output.compression.codec",
	//  "org.apache.hadoop.io.compress.BZip2Codec");
    Job job1 = Job.getInstance(conf, "wordcount");
    for (int i = 0; i < args.length; i += 1) {
      if ("-skip".equals(args[i])) {
        job1.getConfiguration().setBoolean("wordcount.skip.patterns", true);
        i += 1;
        job1.addCacheFile(new Path(args[i]).toUri());
        // this demonstrates logging
        LOG.info("Added file to the distributed cache: " + args[i]);
      }
    }
    job1.setJarByClass(WordCount.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job1.setMapperClass(Map.class);
    job1.setCombinerClass(Reduce.class);
    job1.setReducerClass(Reduce.class);
    job1.setNumReduceTasks(10);
    
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(IntWritable.class);
    
    FileInputFormat.setInputPaths(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, new Path(args[1], "out1"));
    
    job1.waitForCompletion(true);
    
    //job1.waitForCompletion(true);
    
    Job job2 = Job.getInstance(conf, "sort by value");
    for (int i = 0; i < args.length; i += 1) {
        if ("-skip".equals(args[i])) {
          job2.getConfiguration().setBoolean("wordcount.skip.patterns", true);
          i += 1;
          job2.addCacheFile(new Path(args[i]).toUri());
          // this demonstrates logging
          LOG.info("Added file to the distributed cache: " + args[i]);
        }
      }
    job2.setJarByClass(WordCount.class);
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    job2.setMapperClass(MapSwapKeyValue.class);
    job2.setReducerClass(ReduceSwapKeyValue.class);
    job2.setNumReduceTasks(1);
    
    job2.setOutputKeyClass(IntWritable.class);
    job2.setOutputValueClass(Text.class);
    // set key and value as text input format
    job2.setInputFormatClass(KeyValueTextInputFormat.class);
    
    FileInputFormat.setInputPaths(job2, new Path(args[1], "out1"));
    FileOutputFormat.setOutputPath(job2, new Path(args[1], "out2"));
    
    
    return job2.waitForCompletion(true) ? 0 : 1;
  }

  public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
	private String regx = "[a-zA-Z]+";
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private boolean caseSensitive = false;
    private long numRecords = 0;
    private String input;
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

    protected void setup(Mapper.Context context)
        throws IOException,
        InterruptedException {
      if (context.getInputSplit() instanceof FileSplit) {
        this.input = ((FileSplit) context.getInputSplit()).getPath().toString();
      } else {
        this.input = context.getInputSplit().toString();
      }
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
      if (config.getBoolean("wordcount.skip.patterns", false)) {
        URI[] localPaths = context.getCacheFiles();
        parseSkipFile(localPaths[0]);
      }
    }

    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }
    
    @Override
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String line = lineText.toString();
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      Text currentWord = new Text();
      for (String word : WORD_BOUNDARY.split(line)) {
        if (word.isEmpty() || patternsToSkip.contains(word)) {
            continue;
        }
        if(word.matches(regx)){
        	currentWord = new Text(word);
            context.write(currentWord,one);
        }
      }             
    }
  }
  
  public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
    @Override
    public void reduce(Text word, Iterable<IntWritable> counts, Context context)
        throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable count : counts) {
        sum += count.get();
      }
      context.write(word, new IntWritable(sum));
    }
  }
  
  public static class MapSwapKeyValue extends Mapper<Text, Text, IntWritable, Text> {
    @Override
    public void map(Text key, Text value, Context context ) throws IOException,InterruptedException{
    	//swap key-value make freq as key
    	int intValue = Integer.parseInt(value.toString());
    	//is stopword?
    	if(intValue > 4000)
    		context.write(new IntWritable(intValue), key);
    }
  }
  
  public static class ReduceSwapKeyValue extends Reducer<IntWritable, Text, Text, IntWritable> {	
	@Override
    public void reduce(IntWritable key, Iterable<Text> words, Context context)
        throws IOException, InterruptedException {
		//for each freq we have a list of words so write in text a pair of <frequency , word >
		for(Text word : words){
			context.write(new Text(word),key);
		}
    }
  }
  
}